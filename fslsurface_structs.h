/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */
#ifndef fslsurface_STRUCTS_H
#define fslsurface_STRUCTS_H

#include <cmath>

namespace fslsurface_name{
  enum MarchingCubesMode {EQUAL_TO, GREATER_THAN, GREATER_THAN_EQUAL_TO};

  struct float4{
    float4(float xin, float yin, float zin, float win)
    {
      x=xin;
      y=yin;
      z=zin;
      w=win;

    }
    float4()
    {

    }
    float x,y,z,w;
  };

  template<class T>
  struct vec3 {
    vec3(const vec3<T> & v)
    {
      x=v.x;
      y=v.y;
      z=v.z;
    }
    vec3(T xin, T yin, T zin)
    {
      x=xin;
      y=yin;
      z=zin;
    }
    vec3()
    {

    }
    vec3<T>& operator-=( const vec3<T> &other );
    vec3<T>& operator+=( const vec3<T> &other );

    vec3<T>& operator*=( const vec3<T> &other );
    vec3<T>& operator*=( const T & sc );
    vec3<T>& operator/=( const T & sc );

    const vec3<T> operator*(const T &other) const;
    const vec3<T> operator+(const vec3<T> &other) const;
    const vec3<T> operator-(const vec3<T> &other) const;

    void normalize();
    double norm();
    T x,y,z;
  };

  template<class T>
  void vec3<T>::normalize()
  {
    T l2 = (this->x * this->x) +  (this->y * this->y) + (this->z * this->z) ;

    *this /= sqrt(l2);
  }

  template<class T>
  double vec3<T>::norm()
  {
    T l2 = (this->x * this->x) +  (this->y * this->y) + (this->z * this->z) ;
    return static_cast<double>(sqrt(l2));
  }

  template<class T>
  vec3<T>& vec3<T>::operator-=( const vec3<T> & v1 )
  {
    this->x -= v1.x;
    this->y -= v1.y;
    this->z -= v1.z;

    return *this;
  }

  template<class T>
  vec3<T>& vec3<T>::operator+=( const vec3<T> & v1 )
  {
    this->x += v1.x;
    this->y += v1.y;
    this->z += v1.z;

    return *this;
  }

  template<class T>
  vec3<T>& vec3<T>::operator*=( const vec3<T> & v1 )
  {
    this->x *= v1.x;
    this->y *= v1.y;
    this->z *= v1.z;

    return *this;
  }
  template<class T>
  vec3<T>& vec3<T>::operator*=( const T & sc )
  {
    this->x *= sc;
    this->y *= sc;
    this->z *= sc;

    return *this;
  }
  template<class T>
  vec3<T>& vec3<T>::operator/=( const T & sc )
  {
    this->x /= sc;
    this->y /= sc;
    this->z /= sc;

    return *this;
  }
  template<class T>
  const vec3<T> vec3<T>::operator+(const vec3<T> &other) const {
    return vec3<T>(*this) += other;
  }
  template<class T>
  const vec3<T> vec3<T>::operator-(const vec3<T> &other) const {
    return vec3<T>(*this) -= other;
  }
  template<class T>
  const vec3<T> vec3<T>::operator*(const T &sc) const {
    return vec3<T>(*this) *= sc;
  }

  /*
      template<class T>
      vec3<T> subtract( const vec3<T> & v1, const vec3<T> & v2 )
      {
      return vec3<T>(v1.x-v2.xm,v1.y-v2.y,v1.z - v2.z);
      }
  */

  template <class T>
  struct vertex {

    vertex(T xin, T yin, T zin){
      x=xin;
      y=yin;
      z=zin;
      nx = 0;
      ny = 0;
      nz = 0;
      sc=0;
    }
    vertex(T sc_in){
      x = 0;
      y = 0;
      z = 0;
      nx = 0;
      ny = 0;
      nz = 0;
      sc=sc_in;
    }
    vertex(){

    }

    vec3<T> coord() const;

    T x;
    T y;
    T z;
    T nx;
    T ny;
    T nz;
    T sc;
  };

  template<class T>
  vec3<T> vertex<T>::coord() const
  {
    return vec3<T>(this->x,this->y,this->z);
  }


  struct float3{
    float3(float xin, float yin, float zin)
    {
      x=xin;
      y=yin;
      z=zin;
    }
    float3()
    {

    }
    float x,y,z;
  };


  struct float2{
    float2(float xin, float yin)
    {
      x=xin;
      y=yin;
    }
    float2()
    {

    }
    float x,y;
  };
  struct bool3{
    bool3(bool xin, bool yin, bool zin)
    {
      x=xin;
      y=yin;
      z=zin;
    }
    bool3()
    {

    }
    bool x,y,z;
  };



  struct int2{
    int2(int xin,int yin)
    {
      x=xin;
      y=yin;
    }
    int2()
    {

    }

    float x,y;
  };


  struct image_dims{
    image_dims ( float xs, float ys, float zs, float xd, float yd, float zd )
    {
      xsize=xs;
      ysize=ys;
      zsize=zs;
      xdim=xd;
      ydim=yd;
      zdim=zd;
    }

    float xsize,ysize,zsize;
    float xdim,ydim,zdim;
  };

  struct vertexFloat
  {
    vertexFloat(float sc_in){
      x = 0;
      y = 0;
      z = 0;
      nx = 0;
      ny = 0;
      nz = 0;
      scalar=sc_in;
    }
    vertexFloat(){
      x = 0;
      y = 0;
      z = 0;
      nx = 0;
      ny = 0;
      nz = 0;
      scalar=0;
    }

    float x, y, z;        //Vertex
    float nx, ny, nz;     //Normal
    float scalar;
  };

}
#endif // BRIVIEW_STRUCTS_H
