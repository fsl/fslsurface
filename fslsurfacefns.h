/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */
#ifndef FSLSURFACEFNS_H
#define FSLSURFACEFNS_H

#include <map>
#include <list>
#include <vector>
#include <fslsurface_structs.h>
#include <fslsurface.h>
#include <newimage/newimageall.h>
//template<class T, class T2>
//class fslSurface;

namespace fslsurface_name {

  //-----------------------float3 functions------------------------//

  //some function for float3
  // void normalize( float3 & v1);
  float dot(const vec3<float> & v1, const vec3<float> & v2 );
  vec3<float> cross(const vec3<float> & v1, const vec3<float> & v2 );

  vec3<float> subtract(const vec3<float> & v1, const vec3<float> & v2);
  //------------------------SURFACE XFMS----------------------------//
  template<class T,class T2,class T3>
  void apply_xfm( fslSurface<T,T2>& surf, const std::vector<T3> & xfm );

  template<class T,class T2>
  void apply_xfm( fslSurface<T,T2>& surf, const std::string & xfm );

  template<class T,class T2>
  void apply_warp( fslSurface<T,T2>& surf, const NEWIMAGE::volume4D<float> & warp );

  template <class T, class T2>
  void deformSurface( fslSurface<T,T2>& surf , const unsigned int & N, const T & w_norm, const T & w_tan  );

  //--------------------SCALAR PROCESSING----------------//
  template <class T2>
  void remove_connection( std::vector< std::list<T2> > & connections, const unsigned int & index );
  template <class T2>
  void remove_item( std::list<T2> & connections, const unsigned int & index );

  //computes connections matrix given 2 connection matrices, con1 +con2 gives con 3
  template<class T2>
  std::vector< std::list<T2> > computeNextLevelConnections(  std::vector< std::list<T2> > & connections0, std::vector< std::list<T2> > & connections1 );
  template<class T2>
  std::vector< std::list< std::pair<T2,T2> > > computeNextLevelConnectionsWithMultiWayPoints(  std::vector< std::list<T2> > & connections0, std::vector< std::list<T2> > & connections1 );

  template <class T, class T2>
  // std::list< std::pair<T2,T> >
  void conn2dist(fslSurface<T,T2>& surf, const T2 & vert_index, std::list< std::pair<T2,T2> > & conns, std::vector< std::list< std::pair<T2,T> > > & index_distances );

  template <class T,class T2>
  void cluster( fslSurface<T,T2>& surf , const unsigned int & index, const T & threshold );

  template <class T, class T2>
  void sc_smooth_mean_neighbour( fslSurface<T,T2>& surf , const unsigned int & index );

  template <class T, class T2>
  void sc_smooth_gaussian_geodesic( fslSurface<T,T2>& surf , const unsigned int & index, const T & sigma, const T & extent,bool run4D=false );

  template <class T, class T2>
  void multiplyScalarsByScalars(fslSurface<T,T2>& surf , const unsigned int & index, const fslSurface<T,T2>& surf2 , const unsigned int & index1);

  template <class T, class T2>
  void subtractScalarsFromScalars(fslSurface<T,T2>& surf , const unsigned int & index, const fslSurface<T,T2>& surf2 , const unsigned int & index1, std::string name="sc_sub_sc");//surf-surf2

  //----------------VECTOR PROCESSING------------------//
  template <class T, class T2>
  void projectVectorsOntoNormals(fslSurface<T,T2>& surf ,const unsigned int & index);

  //-----------------MARCHING CUBES---------------------//
  template<class T,class T2, class T3>
  void runMarchingCubesOnAllLabels( fslSurface<T,T2>& surf , const T3* image,  const fslsurface_name::image_dims & dims, const T3 & thresh_in);

  template<class T,class T2, class T3>
  void marchingCubes( fslSurface<T,T2>& surf , const T3* images,  const fslsurface_name::image_dims & dims, const T3 & thresh, const T & label, const fslsurface_name::MarchingCubesMode & mode);


  //--------------------DATA MANIPULATIONS---------------//
  template<class T,class T2>
  void maskScalars( fslSurface<T,T2>& surf , const unsigned int & sc_index, const short* mask,fslsurface_name::image_dims dims, bool  setToCurrentSc = true );
  template<class T,class T2>
  void maskSurface( fslSurface<T,T2>& surf , const short* mask, const fslsurface_name::image_dims dims, bool  setToCurrentSc = true );

  template<class T,class T2, class T3>
  void mapScalars( fslSurface<T,T2>& surf , const unsigned int & sc_index, const std::map<T3,T3> & scMap, bool setToCurrentSc =true );
  //void maskScalars(const unsigned int & sc_index, const short* mask ,const fslsurface_name::image_dims & dims, bool setToCurrentSc =true);

  template< class T >
  void vertexInterp( const float & thresh, const vec3<T> & v1 , const float  & val1, const vec3<T> & v2, const float & val2 , vertex<T> & vout);


  template<class T,class T2, class T3>
  void insertScalars(fslsurface_name::fslSurface<T,T2>& surf , const NEWIMAGE::volume4D<T> & time_series, const std::string & time_name );

  template<class T,class T2>
  void sampleTimeSeries(fslsurface_name::fslSurface<T,T2>& surf , const NEWIMAGE::volume4D<T> & time_series, const std::string & time_name );

  template<class T,class T2>
  void writeTimeSeries( const fslsurface_name::fslSurface<T,T2>& surf , NEWIMAGE::volume4D<T> & time_series , const std::string & outname);

}

#endif
