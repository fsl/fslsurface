/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */
#ifndef _fslSurface_GL
#define _fslSurface_GL

/* This file is not used, but is left for historical reasons. */

#include <fslsurface.h>


#ifdef __linux

#define GL_GLEXT_PROTOTYPES
#include <GL/glut.h>

#else
#include <OpenGL/gl.h>
#include <GLUT/glut.h>


#endif
namespace fslsurface_name{


  void glutInitAndCreateWindow( int *argcp, char **argv, const std::string & name );
  void glutDisp( void );
  // void glutIdle( void );
  void glutRender( const fslSurface<float,unsigned int> & surf, std::string name = "fslSurface GLUT" );

  void keyb(unsigned char key, int x, int y);


  template<class T,class T2>
  void glBufferData_Vertices( const fslsurface_name::fslSurface<T,T2> & surf, const GLuint & vbo ) ;
  template<class T,class T2>
  void glBufferData_Faces( const fslsurface_name::fslSurface<T,T2> & surf, const GLuint & vbo ) ;
  template<class T,class T2>
  void glBufferSubData_Vertices( const fslsurface_name::fslSurface<T,T2> & surf, const GLuint & vbo ) ;
  template<class T,class T2>
  void glBufferSubData_Faces( const fslsurface_name::fslSurface<T,T2> & surf, const GLuint & vbo ) ;
  template<class T,class T2>
  void render( const fslsurface_name::fslSurface<T,T2> & surf, const GLint & vertexLoc , const GLint &  normalLoc, const GLint & scalarLoc, const GLuint & vbo_verts, const GLuint & vbo_ele_array )  ;

  template<class T,class T2>
  void depthSortTriangles( const fslSurface<T,T2> & surf, const GLuint & vbo);


}
#endif
