/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */
#ifndef FSLSURFACE_FIRST_H
#define FSLSURFACE_FIRST_H

//STL INCLUDES
#include <string>
#include <vector>

//FSL INCLUDES
#include "armawrap/newmat.h"
#include <newimage/newimageall.h>
#include "shapeModel/shapeModel.h"

#include <fslsurface_structs.h>

namespace fslsurface_name {
  template<class T, class T2>
  class fslSurface;

  template<class T>
  void draw_segment(NEWIMAGE::volume<short>& image, const fslsurface_name::vec3<T> & p1, const fslsurface_name::vec3<T> &  p2, const int & label);

  template<class T, class T2>
  void draw_mesh(const fslSurface<T,T2> & surf, NEWIMAGE::volume<short>& image, const int & label);

  template<class T, class T2>
  NEWIMAGE::volume<short> fillMesh( const fslSurface<T,T2> & surf, const NEWIMAGE::volume<short> & image, const int & label ) ;


  //  template<class T, class T2>
  //NEWIMAGE::volume<short> fillMesh( const fslSurface<T,T2> & surf, const NEWIMAGE::volume<short> & image, const int & label );


  float MVGLM_fit(NEWMAT::Matrix G, NEWMAT::Matrix D, NEWMAT::Matrix contrast, int& df1, int& df2);
  float MVGLM_fit(NEWMAT::Matrix G, NEWMAT::Matrix D, NEWMAT::Matrix contrast);
  std::vector<float> FtoP( const std::vector<float> & F, const int & dof1, const int & dof2);


  SHAPE_MODEL_NAME::shapeModel* loadAndCreateShapeModel( const std::string & modelname);
  void readBvars(const std::string & filename, const std::string & image_path, std::string & modelname, unsigned int & Nsubjects, std::vector<std::string> & v_imagenames, std::vector< std::vector<float> > & v_bvars, std::vector< std::vector<float> > & v_xfms);

  std::vector<float> getFSLtoNIFTIxfm( const NEWIMAGE::volume<float> & imref);


  void reconAllSurfacesAndSave( const std::string & filename, const std::string & out_base  );
  void reconSurface_from_bvars( fslSurface<float,unsigned int> & surf, const std::string & filename);
  void reconSurfaces_from_bvars( std::vector< fslSurface<float,unsigned int> > & v_surf, const std::string & filename, const std::string & image_path, const std::string & template_name, std::string out_base = "NULL",  bool reconMNI=false );




  std::vector<float> meshRegLeastSq( const fslSurface<float,unsigned int> &  m_src, const fslSurface<float,unsigned int> & m_target, const unsigned int & dof );
  void vertexMVglm( fslSurface<float,unsigned int> & surf_stats, const std::string & inname, const std::string & designname, const std::string & saveVertices, std::string normname );

  //transformation matrix utilities
  void preMultiplyGlobalRotation(NEWMAT::Matrix & fmat, const NEWMAT::Matrix & R);
  void preMultiplyGlobalScale(NEWMAT::Matrix & fmat, const float & s);
  void preMultiplyGlobalScale(NEWMAT::Matrix & fmat, const float & sx,const float & sy, const float & sz);
  void preMultiplyTranslation(NEWMAT::Matrix & fmat, const  float & tx, const float & ty, const float & tz );
  NEWMAT::ReturnMatrix getIdentityMatrix(const short N);

  std::vector<double> invertMatrix( const std::vector<double> & fmat);

}
#endif
