/*    Copyright (C) 2012 University of Oxford  */

/*  CCOPYRIGHT  */
#include <fslsurface.h>
#include <iostream>
using namespace fslsurface_name;
using namespace std;
int main(int argc, char* argv[]){
  try {
    cout<<"Read file "<<argv[1]<<endl;
    fslSurface<float,int>* surf = new fslSurface<float,int>(argv[1]);

    delete surf;

  }
  catch (std::exception &e) {
    cerr << e.what() << endl;

  }



}
