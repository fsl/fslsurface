#!/bin/sh
#   Copyright (C) 2012 University of Oxford
#
#   SHCOPYRIGHT
useBvars=0;
doVertexAnalysis=0;
infile=""
reffile=""
outname=""
imagepath=""
designame=""
DOF=0;
DEBUG=0;

#if 0 
reconMode=0;


while [ $# -gt 0 ] ; do 
	if [ $1 = "--vertexAnalysis" ] ; then 
		doVertexAnalysis=1
		shift
	elif [ $1 = "--usebvars" ] ; then 
		useBvars=1
		shift
	elif [ $1 = "--useReconNative" ] ; then 
		reconMode=1
		shift
	elif [ $1 = "--useReconMNI" ] ; then 
		reconMode=2
		shift
	elif  [ $1 = "-i" ] ; then 
		infile=$2
		shift 2
    elif  [ $1 = "-r" ] ; then 
        reffile=$2
        shift 2
	elif  [ $1 = "-d" ] ; then 
		designame=$2
		shift 2
	elif  [ $1 = "-o" ] ; then 
		outname=$2
		shift 2
	elif  [ $1 = "-a" ] ; then 
		imagepath="-a $2"
		shift 2
	elif  [ $1 = "--useRigidAlign" ] ; then 
		DOF=`echo "${DOF}+6" | bc`
		shift 1
	elif  [ $1 = "--useScale" ] ; then 
		DOF=`echo "${DOF}+1" | bc`
		shift 1
	elif  [ $1 = "--debug" ] ; then 
        echo "debug"
		DEBUG=1
		shift 1
	else
		echo "Unrecognized option $1"
		exit 1;
	fi

done
#Done running analysis
	if [ $doVertexAnalysis -eq 1 ] ;then 
#if [ $useBvars -eq 1 ] ; then
		
			if [ -f  ${outname}_surfaces_aligned.txt ] ; then 
				rm ${outname}_surfaces_aligned.txt
			fi
		   
			echo "Reconstruct Data from bvars... $imagepath"
            if [ $useBvars = 1 ] ; then 
                        if [ $reconMode -eq 0 ] ; then 
                            echo "Please choose the space to reconstruct surfaces into"
                            exit 1
                        elif [ $reconMode -eq 1 ] ; then 
                            echo "do reconMode"
            #./fslsurface_utils --mni_template=${FSLDIR}/data/standard/MNI152_T1_1mm --doReconSurfacesFromBvars  $imagepath -s $infile -o $outname 
                             ${FSLDIR}/bin/fslsurfacemaths -reconAllFromBvarsAndSave $infile $outname 
                                reffile="${outname}_modelmean.gii"
                         elif [ $reconMode -eq 2 ] ; then 
                            echo "reecon mni"
            #./fslsurface_utils --mni_template=${FSLDIR}/data/standard/MNI152_T1_1mm --doReconSurfacesFromBvars --reconMNISpace $imagepath -s $infile -o $outname 

                          fi 
            else
                echo "copy file"
                cp $infile ${outname}_list.txt


            fi
			for surf in `cat ${outname}_list.txt` ; do 
				echo "Registering surface ${surf}..." 
#				echo ${fsurf}_2_mni_dof${DOF}.gii
				fsurf=`basename $surf .gii`
                fsurf=`basename $surf .vtk`
				dsurf=`dirname $surf`
			#	echo ${fsurf}_to_mni_dof${DOF}.gii

				if [ ${DOF} -ge 6 ] ; then 
					#./build/Debug/fslsurface_utils --doLeastSquaresReg -s $surf -r ${outname}_mean_mni.gii -o ${fsurf}_2_mni_dof${DOF}.gii -d ${DOF}
                    fsurf=`basename $surf .gii`
                    ${FSLDIR}/bin/fslsurfacemaths $surf -reg_lq ${reffile} ${DOF} ${dsurf}/${fsurf}_to_mni_dof${DOF}.mat  ${dsurf}/${fsurf}_to_mni_dof${DOF}.gii					

                    echo "${SURFDIR}/fslsurfacemaths $surf -reg_lq ${outname}_modelmean.gii ${DOF} ${fsurf}_to_mni_dof${DOF}.mat  ${fsurf}_to_mni_dof${DOF}.gii"
#./fslsurface_utils --doLeastSquaresReg -s $surf -r ${outname}_mean_mni.gii -o ${dsurf}/${fsurf}_to_mni_dof${DOF}.gii -d ${DOF}
			#		echo "DOF ${DOF}"
					echo ${dsurf}/${fsurf}_to_mni_dof${DOF}.gii >> ${outname}_surfaces_aligned.txt
				else	
			#	echo "dof0 $surf ${dsurf}/${fsurf}_to_mni_dof0.gii"
					mv $surf  ${dsurf}/${fsurf}_to_mni_dof0.gii
					echo  ${dsurf}/${fsurf}_to_mni_dof${DOF}.gii >> ${outname}_surfaces_aligned.txt
				fi
			
			done
echo "run vetex MVGLM "


			 ${FSLDIR}/bin/fslsurfacemaths -vertexMVGLM ${outname}_surfaces_aligned.txt ${designame} none ${outname}_F.gii
			echo "done fslSurfaceutils vertex analysis"
#			./build/Debug/fslsurface_utils  --doVertexGLMfit -i ${outname}_surfaces_aligned.txt
#	./fslsurface_utils  --doVertexGLMfit --designame=${designame} -s ${outname}_surfaces_aligned.txt -o ${outname}_F
		
            echo DEBUG $DEBUG
			if [ $DEBUG = 0 ] ;then
                echo clean
                if [ $useBvars = 1 ] ; then
                                rm `cat ${outname}_surfaces_aligned.txt` `cat ${outname}_list.txt`
                fi
            fi
		
#fi
	fi
	
	
	
