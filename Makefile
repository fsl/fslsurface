include ${FSLCONFDIR}/default.mk

PROJNAME = fslsurface
XFILES   = fslsurfacemaths
SOFILES  = libfsl-surface.so libfsl-surface_backcompat.so
LIBS     = -lfsl-shapeModel -lfsl-vtkio -lfsl-meshclass -lfsl-giftiio \
           -lfsl-first_lib -lfsl-newimage -lfsl-miscmaths -lfsl-NewNifti \
           -lfsl-znz -lfsl-utils -lfsl-cprob -lexpat

all: ${SOFILES} ${XFILES}

libfsl-surface.so: fslsurfacefns.o fslsurfaceio.o fslsurface.o
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

libfsl-surface_backcompat.so: fslsurface_first.o fslsurface_dataconv.o libfsl-surface.so
	${CXX} ${CXXFLAGS} -shared -o $@ $^ ${LDFLAGS}

fslsurfacemaths: fslsurfacemaths.o fslsurfaceio.o fslsurfacefns.o fslsurface_first.o fslsurface.o
	${CXX} ${CXXFLAGS} -o $@ $^ -lfsl-surface_backcompat -lfsl-surface ${LDFLAGS}
